<?php
// ==============================================================================
//	Export CSV Files of Orders and Customers 
// ==============================================================================
add_action('admin_menu', 'nutrabox_register_csv_export_submenu_page');
function nutrabox_register_csv_export_submenu_page() {
	if( is_user_logged_in() && is_admin() ){
    	add_menu_page('CSV Export', 'CSV Export', 'manage_options', 'nutraboxCSVExport', 'Nutrabox_CSV_Export_link' ); 
	}
}

// This function will create buttons to genrate csv of orders and customers
function Nutrabox_CSV_Export_link() {
    echo '<h1>Export Customers & Orders</h1>'; 
    ?>
    <form method="post">
        <p class="submit">
            <input type="submit" name="user" id="user" class="button button-primary" value="Genrate CSV of Customers"  />
        </p>
    </form>
    
    <form method="post">
        <p class="submit">
            <input type="submit" name="order" id="order" class="button button-primary" value="Genrate CSV of Orders"  />
        </p>
    </form>
    <?php 
    if (isset($_POST['user'])) { 
        return NutraboxCreatCSVUser();
    }
    if (isset($_POST['order'])) { 
        return NutraboxCreatCSVOrder();
    }     
}
// This function will create csv of users.
function NutraboxCreatCSVUser() {
    ob_end_clean();
    $domain = $_SERVER['SERVER_NAME'];
    $filename = 'users-' . $domain . '-' . date('Y-M-d') . '.csv';
    $header_row = array(
        'ID',
        'Login Name',
        'User password',
        'Nick Name',
        'User Email',
        'User URL',
        'Register Date',
        'Activation Key',
        'Status',
        'Display Name'
    );
    $data_rows = array();
    global $wpdb;
    $prefix = $wpdb->prefix;
    $sql = 'SELECT * FROM ' . $wpdb->users;
    $users = $wpdb->get_results($sql, 'ARRAY_A');
    foreach($users as $user) {
        $row = $user;
        $data_rows[] = $row;
    }
    fprintf($fh, chr(0xEF) . chr(0xBB) . chr(0xBF));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Content-Description: File Transfer');
    header('Content-type: text/csv');
    header("Content-Disposition: attachment; filename={$filename}");
    header('Expires: 0');
    header('Pragma: public');
    $fh = @fopen('php://output', 'w');
    fputcsv($fh, $header_row);
    foreach($data_rows as $data_row) {
        fputcsv($fh, $data_row);
    }
    fclose($fh);
    die();
}
// This function will create csv of orders.
function NutraboxCreatCSVOrder(){
    echo get_stylesheet_directory_uri();echo'<br>';
    echo get_template_directory_uri();echo'<br>';
    echo get_stylesheet_directory();
}